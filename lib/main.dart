import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Gustav Becker Calculator',
      theme: ThemeData(brightness: Brightness.dark),
      home: CalculatorView(title: 'Gustav Becker Calculator'),
    );
  }
}

class CalculatorView extends StatefulWidget {
  final String title;

  const CalculatorView({Key key, this.title}) : super(key: key);
  @override
  _CalculatorViewState createState() => _CalculatorViewState();
}

class _CalculatorViewState extends State<CalculatorView> {
  final _formKey = GlobalKey<FormState>();
  final serialNumber = TextEditingController();
  String result = '';
  String _forumUrl =
      'https://mb.nawcc.org/threads/post-your-gustav-becker-clocks-here.10545/page-2#post-71775';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          centerTitle: true,
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.teal,
                ),
                child: Text(
                  'About this project',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.functions),
                title: Text('How it is calculated'),
                onTap: () {
                  _showHowItIsCalculate();
                },
              ),
              ListTile(
                leading: Icon(Icons.source),
                title: Text('Credits'),
                onTap: () {
                  _showSources();
                },
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _buildTitle(),
                _buildResultText(),
                Divider(
                  thickness: 3,
                  indent: 25,
                  endIndent: 25,
                  height: 50,
                ),
                _buildForm(),
                _buildButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _buildTitle() {
    return SizedBox(
      width: MediaQuery.of(context).size.width / 1.2,
      child: TypewriterAnimatedTextKit(
        isRepeatingAnimation: false,
        onTap: () {
          print("Tap Event");
        },
        text: [
          "Your clock was made in:",
        ],
        textStyle: TextStyle(
            fontSize: 30.0, fontFamily: "RobotoMono", color: Colors.white),
        textAlign: TextAlign.center,
      ),
    );
  }

  _buildResultText() {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: Text(
        '${result.toString()}',
        style: TextStyle(
            fontSize: 20, fontFamily: "RobotoMono", color: Colors.white),
        textAlign: TextAlign.center,
      ),
    );
  }

  _buildForm() {
    return Form(
      key: _formKey,
      child: TextFormField(
        autofocus: true,
        controller: serialNumber,
        keyboardType: TextInputType.numberWithOptions(),
        textInputAction: TextInputAction.done,
        decoration: InputDecoration(
          hintText: 'Enter the serial number here',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value.isEmpty) {
            return 'Please enter a serial number';
          }
          if (value.length < 7 || value.length > 7) {
            return 'Serial numbers are made from 7 numbers, please check your serialnumber and try again.';
          }
          return null;
        },
      ),
    );
  }

  _buildButton() {
    return Padding(
      padding: const EdgeInsets.only(top: 15),
      child: ElevatedButton(
        onPressed: () {
          if (_formKey.currentState.validate()) {
            _calculate(serialNumber.text);
            print(serialNumber.text);
          }
        },
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Colors.teal),
        ),
        child: Text('Calculate'),
      ),
    );
  }

  _showHowItIsCalculate() {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('How it is calculated'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'All serial numbers are compared to a table John Hubby has made.'),
                Text('The table can be found on the NAWCC forums.'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Show table on forum'),
              onPressed: () {
                _launchURL();
              },
            ),
            TextButton(
              child: Text('Close'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _showSources() {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Credits'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Programmer: Cedric Hannecart'),
                Text('Data Sources:'),
                Text('John Hubby, Ferdi Hannecart, ...'),
                // Text(''),
                // Text('If ')
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Close'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  //=================
  void _launchURL() async => await canLaunch(_forumUrl)
      ? await launch(_forumUrl)
      : throw 'Could not launch $_forumUrl';

  _calculate(String serialNumber) {
    // int sn = int.parse(serialNumber);
    int sn = int.parse(serialNumber);
    String prefix = '';
    String postfix = '';
    setState(() {});
    if (sn < 000005) {
      result = '$prefix 1847 $postfix';
    }
    if (sn >= 0000051 && sn < 0000121) {
      result = '$prefix 1848 $postfix';
    }
    if (sn >= 0000121 && sn < 0000251) {
      result = '$prefix 1850 $postfix';
    }
    if (sn >= 0000251 && sn < 0000481) {
      result = '$prefix 1851 $postfix';
    }
    if (sn >= 0000481 && sn < 0000721) {
      result = '$prefix 1852 $postfix';
    }
    if (sn >= 0000481 && sn < 0000721) {
      result = '$prefix 1852 $postfix';
    }
    if (sn >= 0000721 && sn < 0000971) {
      result = '$prefix 1853 $postfix';
    }
    if (sn >= 0000971 && sn < 0001241) {
      result = '$prefix 1854 $postfix';
    }
    if (sn >= 0001241 && sn < 0001541) {
      result = '$prefix 1855 $postfix';
    }
    if (sn >= 0001541 && sn < 0001881) {
      result = '$prefix 1856 $postfix';
    }
    if (sn >= 0001881 && sn < 0002271) {
      result = '$prefix 1857 $postfix';
    }
    if (sn >= 0002271 && sn < 0002721) {
      result = '$prefix 1858 $postfix';
    }
    if (sn >= 0002721 && sn < 0003251) {
      result = '$prefix 1859 $postfix';
    }
    if (sn >= 0003251 && sn < 0003901) {
      result = '$prefix 1860 $postfix';
    }
    if (sn >= 0003901 && sn < 0005101) {
      result = '$prefix 1861 $postfix';
    }
    if (sn >= 0005101 && sn < 0006901) {
      result = '$prefix 1862 $postfix';
    }
    if (sn >= 0006901 && sn < 0009301) {
      result = '$prefix 1863 $postfix';
    }
    if (sn >= 0009301 && sn < 0011901) {
      result = '$prefix 1864 $postfix';
    }
    if (sn >= 0011901 && sn < 0014801) {
      result = '$prefix 1865 $postfix';
    }
    if (sn >= 0011901 && sn < 0018001) {
      result = '$prefix 1866 $postfix';
    }
    if (sn >= 0018001 && sn < 0021701) {
      result = '$prefix 1867 $postfix';
    }
    if (sn >= 0021701 && sn < 0025901) {
      result = '$prefix 1868 $postfix';
    }
    if (sn >= 0025901 && sn < 0030601) {
      result = '$prefix 1869 $postfix';
    }
    if (sn >= 0030601 && sn < 0035801) {
      result = '$prefix 1870 $postfix';
    }
    if (sn >= 0035801 && sn < 0041801) {
      result = '$prefix 1871 $postfix';
    }
    if (sn >= 0041801 && sn < 0049501) {
      result = '$prefix 1872 $postfix';
    }
    if (sn >= 0049501 && sn < 0059901) {
      result = '$prefix 1873 $postfix';
    }
    if (sn >= 0059901 && sn < 0073001) {
      result = '$prefix 1874 $postfix';
    }
    if (sn >= 0073001 && sn < 0089301) {
      result = '$prefix 1875 $postfix';
    }
    if (sn >= 0089301 && sn < 0109001) {
      result = '$prefix 1876 $postfix';
    }
    if (sn >= 0109001 && sn < 0132401) {
      result = '$prefix 1877 $postfix';
    }
    if (sn >= 0132401 && sn < 0159901) {
      result = '$prefix 1878 $postfix';
    }
    if (sn >= 0159901 && sn < 0192001) {
      result = '$prefix 1879 $postfix';
    }
    if (sn >= 0192001 && sn < 0228701) {
      result = '$prefix 1880 $postfix';
    }
    if (sn >= 0228701 && sn < 0270001) {
      result = '$prefix 1881 $postfix';
    }
    if (sn >= 0270001 && sn < 0316001) {
      result = '$prefix 1882 $postfix';
    }
    if (sn >= 0316001 && sn < 0365001) {
      result = '$prefix 1883 $postfix';
    }
    if (sn >= 0365001 && sn < 0416001) {
      result = '$prefix 1884 $postfix';
    }
    if (sn >= 0416001 && sn < 0469001) {
      result = '$prefix 1885 $postfix';
    }
    if (sn >= 0469001 && sn < 0526001) {
      result = '$prefix 1886 $postfix';
    }
    if (sn >= 0526001 && sn < 0588001) {
      result = '$prefix 1887 $postfix';
    }
    if (sn >= 0588001 && sn < 0656001) {
      result = '$prefix 1888 $postfix';
    }
    if (sn >= 0656001 && sn < 0729001) {
      result = '$prefix 1889 $postfix';
    }
    if (sn >= 0729001 && sn < 0805001) {
      result = '$prefix 1890 $postfix';
    }
    if (sn >= 0805001 && sn < 0882001) {
      result = '$prefix 1891 $postfix';
    }
    if (sn >= 0882001 && sn < 0958001) {
      result = '$prefix 1892 $postfix';
    }
    if (sn >= 0958001 && sn < 1030001) {
      result = '$prefix 1893 $postfix';
    }
    if (sn >= 1030001 && sn < 1096001) {
      result = '$prefix 1894 $postfix';
    }
    if (sn >= 1096001 && sn < 1156001) {
      result = '$prefix 1895 $postfix';
    }
    if (sn >= 1156001 && sn < 1212001) {
      result = '$prefix 1896 $postfix';
    }
    if (sn >= 1212001 && sn < 1265001) {
      result = '$prefix 1897 $postfix';
    }
    if (sn >= 1265001 && sn < 1316001) {
      result = '$prefix 1898 $postfix';
    }
    if (sn >= 1316001 && sn < 1366001) {
      result = '$prefix 1899 $postfix';
    }
    if (sn >= 1366001 && sn < 1441001) {
      result = '$prefix 1900 $postfix';
    }
    if (sn >= 1441001 && sn < 1530001) {
      result = '$prefix 1901 $postfix';
    }
    if (sn >= 1530001 && sn < 1610001) {
      result = '$prefix 1902 $postfix';
    }
    if (sn >= 1610001 && sn < 1683001) {
      result = '$prefix 1903 $postfix';
    }
    if (sn >= 1683001 && sn < 1753001) {
      result = '$prefix 1904 $postfix';
    }
    if (sn >= 1753001 && sn < 1821001) {
      result = '$prefix 1905 $postfix';
    }
    if (sn >= 1821001 && sn < 1887001) {
      result = '$prefix 1906 $postfix';
    }
    if (sn >= 1887001 && sn < 1951001) {
      result = '$prefix 1907 $postfix';
    }
    if (sn >= 1951001 && sn < 2013001) {
      result = '$prefix 1908 $postfix';
    }
    if (sn >= 2013001 && sn < 2073001) {
      result = '$prefix 1909 $postfix';
    }
    if (sn >= 2073001 && sn < 2130001) {
      result = '$prefix 1910 $postfix';
    }
    if (sn >= 2130001 && sn < 2184001) {
      result = '$prefix 1911 $postfix';
    }
    if (sn >= 2184001 && sn < 2231501) {
      result = '$prefix 1912 $postfix';
    }
    if (sn >= 2231501 && sn < 2266001) {
      result = '$prefix 1913 $postfix';
    }
    if (sn >= 2266001 && sn < 2285001) {
      result = '$prefix 1914 $postfix';
    }
    if (sn >= 2285001 && sn < 2297001) {
      result = '$prefix 1915 $postfix';
    }
    if (sn >= 2297001 && sn < 2306001) {
      result = '$prefix 1916 $postfix';
    }
    if (sn >= 2306001 && sn < 2315001) {
      result = '$prefix 1917 $postfix';
    }
    if (sn >= 2315001 && sn < 2331001) {
      result = '$prefix 1918 $postfix';
    }
    if (sn >= 2331001 && sn < 2349001) {
      result = '$prefix 1919 $postfix';
    }
    if (sn >= 2349001 && sn < 2370001) {
      result = '$prefix 1920 $postfix';
    }
    if (sn >= 2370001 && sn < 2393001) {
      result = '$prefix 1921 $postfix';
    }
    if (sn >= 2393001 && sn < 2418001) {
      result = '$prefix 1922 $postfix';
    }
    if (sn >= 2418001 && sn < 2444001) {
      result = '$prefix 1923 $postfix';
    }
    if (sn >= 2444001 && sn < 2470001) {
      result = '$prefix 1924 $postfix';
    }
    if (sn >= 2470001 && sn < 2496001) {
      result = '$prefix 1925 $postfix';
    }
    if (sn >= 2496001 && sn < 2510001) {
      result = '$prefix the first half of 1925 $postfix';
    }
  }
}
