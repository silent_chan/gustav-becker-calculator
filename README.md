# Gustav Becker Calculator

Project made with research data gathered by John Hubby. 

## Getting Started

This project is written in flutter, if you have flutter installed clone the repo & run `flutter run` in the project directory. 

The project can be compiled to all platforms, but since I do not have apple devices I have not confirmed that it will run native on those platforms.

I verified it works in the browser & android.
